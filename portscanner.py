import socket

for port in range(1, 1025):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(0.5)

    ip=socket.gethostbyname("www.noonacademy.com")#converting hostname to ip address
    result = sock.connect_ex((ip, port))
    if 0 == result:
        print("Port: {} Open".format(port))
    sock.close()